gitdata
=======

.. testsetup::

    from gitdata import *

.. automodule:: gitdata
    :members:
    :undoc-members:
    :special-members: __init__, __len__
