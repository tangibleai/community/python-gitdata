from .. import init


def test_init():
    assert init([b"a", b"bc", b"abc"]) == b"abc"
