__version__ = "0.0.0"

from .gitdata import init

__all__ = [
    "init",
]
