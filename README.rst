========
Overview
========

git lfs + private data for FOSS projects so that you can .gitignore private notes, documentation, .env files and large
files while still backing them up to private cloud storage.

* Free software: GNU Lesser General Public License v3 or later (LGPLv3+)

Installation
============

::

    pip install python-gitdata

You can also install the in-development version with::

    pip install https://gitlab.com/hobs/python-gitdata/-/archive/main/python-gitdata-main.zip


Documentation
=============


https://python-gitdata.readthedocs.io/


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
